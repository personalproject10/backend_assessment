// [Section] Dependecies and Modules
const User = require("../models/user");
const bcrypt = require("bcrypt");
const dotenv = require("dotenv");
const auth = require("../auth");

// [Section] Environment Variable Setup
dotenv.config();
const salt = Number(process.env.SALT);

// [Section] Functionalities

// User Registration
module.exports.register = (req, res) => {
  return User.findOne({ email: req.body.email })
    .then((result) => {
      if (result) {
        res.send({
          message: `The email address ${req.body.email} already in use.`,
        });
      } else {
        let newUser = new User({
          email: req.body.email,
          password: bcrypt.hashSync(req.body.password, salt),
        });
        return newUser.save().then((result) => {
          res.send(`UserId: ${result._id} Balance:${result.accountBalance}`);
        });
      }
    })
    .catch((err) => res.send(err));
};

// Login
module.exports.login = (req, res) => {
  return User.findOne({ email: req.body.email })
    .then((result) => {
      if (result === null) {
        res.send({
          message: `No user with ${req.body.email} email address found `,
        });
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          req.body.password,
          result.password
        );
        if (isPasswordCorrect) {
          res.send({ accessToken: auth.createAccessToken(result.toObject()) });
        } else {
          res.send({ message: `Failed to login. Password incorrect` });
        }
      }
    })
    .catch((err) => res.send(err));
};

// Deposit
module.exports.deposit = (req, res) => {
  const deposit = req.body.deposit;
  const account = req.body.userId;

  if (deposit <= 0) {
    res.send({ message: `Deposit amount should not be lower than 0` });
  } else {
    return User.findById(account)
      .then((result, error) => {
        if (res) {
          let newBalance = deposit + result.accountBalance;
          let updatedUser = {
            accountBalance: newBalance,
          };
          return User.findByIdAndUpdate(account, updatedUser).then(
            (result2) => {
              if (result2) {
                res.send({
                  message: `Successfully deposited the amount ${deposit} to User Id ${account}. New balance is ${newBalance}`,
                });
              } else {
                res.send({ message: `Failed to transact. Try again!` });
              }
            }
          );
        } else {
          res.send({
            message: `User Id ${account} not found! Please indicate the right User Id`,
          });
        }
      })
      .catch((err) => res.send(false));
  }
};

// Withdraw
module.exports.withdraw = (req, res) => {
  const withdraw = req.body.withdraw;
  const account = req.params.userId;

  return User.findById(account)
    .then((result) => {
      if (withdraw > result.accountBalance) {
        res.send({
          message: `Sorry! You cannot withdraw more than your available balance.`,
        });
      } else {
        let newBalance = result.accountBalance - withdraw;
        let updatedUser = {
          accountBalance: newBalance,
        };
        return User.findByIdAndUpdate(account, updatedUser).then((result2) => {
          if (result2) {
            res.send({
              message: `Successfully withdrawn the amount ${withdraw}. New balance is ${newBalance}`,
            });
          } else {
            res.send({ message: `Failed to transact. Try again!` });
          }
        });
      }
    })
    .catch((err) => res.send(false));
};

// Check Balance
module.exports.balance = (req, res) => {
  return User.findById(req.params.userId)
    .then((result) => {
      res.send({ message: `Current balance is ${result.accountBalance}` });
    })
    .catch((err) => res.send(err));
};
