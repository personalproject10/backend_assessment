// [Section] Modules and Dependencies
const mongoose = require("mongoose");

// [Section] Schema/Blueprint

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "Email is Required"],
  },
  password: {
    type: String,
    required: [true, "Password is Required"],
  },
  accountBalance: {
    type: Number,
    default: 0,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
});

// [Section] Model
module.exports = mongoose.model("User", userSchema);
