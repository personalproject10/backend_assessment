// [Section] Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require("cors");
const userRoutes = require("./routes/user");

// [Section] Environment Setup
dotenv.config();

let account = process.env.CREDENTIALS;
const port = process.env.PORT;

// [Section] Server Setup
const app = express();
app.use(express.json());
app.use(cors());

// [Section] Database Connection
mongoose.connect(account);
const connectStatus = mongoose.connection;
connectStatus.once("open", () =>
  console.log("Connected to Backend Assessment cloud database")
);

// [Section] Backend Routes
app.use("/users", userRoutes);

// [Section] Server Gateway Response
app.get("/", (req, res) => {
  res.send("Welcome to the Backend Assessment App");
});
app.listen(port, () => {
  console.log("Initializing. Server running.");
});
