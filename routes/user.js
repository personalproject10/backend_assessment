// [Section] Dependencies and Modules
const exp = require("express");
const controller = require("../controllers/user");
const auth = require("../auth");

// [Section] Routing Component
const route = exp.Router();

// [Section] Routes

// Registration
route.post("/register", controller.register);

// Login
route.post("/login", controller.login);

// Deposit
route.put("/:userId/deposit", auth.verifyUser, controller.deposit);

//  Withdraw
route.put("/:userId/withdraw", auth.verifyUser, controller.withdraw);

// Check Balance
route.get("/:userId", auth.verifyUser, controller.balance);

module.exports = route;
